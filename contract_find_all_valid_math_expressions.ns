/** @param {NS} ns **/

// usage: 
// run (scriptname) [first arg] [second arg]
// first arg = number to work with
// second arg = target number

// Find All Valid Math Expressions
// You are attempting to solve a Coding Contract. You have 10 tries remaining, after which the contract will self-destruct.
// You are given the following string which contains only digits between 0 and 9:
// 456409316
// You are also given a target number of -79. Return all possible ways you can add the +, -, and * operators to the string such that it evaluates to the target number.
// The provided answer should be an array of strings containing the valid expressions. The data provided by this problem is an array with two elements. 
// The first element is the string of digits, while the second element is the target number:
// ["456409316", -79]
// NOTE: Numbers in the expression cannot have leading 0's. In other words, "1+01" is not a valid expression Examples:
// Input: digits = "123", target = 6
// Output: [1+2+3, 1*2*3]
// Input: digits = "105", target = 5
// Output: [1*0+5, 10-5]

// we take the string and convert it into an array of numbers
// we create a base 3 number for the 3 math operators +, - and *
// we go through each combination of math operators between the numbers and find out which combinations give the desired result
// we save the results in an array and print it to the terminal

var debug = false

export async function main(ns) {
	// var arrayInput = ["456409316", -79]
	// var arrayInput = ["123", 6]
	var arrayInput = ["105", 5]
	var debugFile = "debug.txt"
	if (debug) await ns.write(debugFile, "contract: find all valid math expressions", "w")
	if (debug) await ns.write(debugFile, "\n\n")

	var numbersString, targetNumber
	if (ns.args[0] !== undefined) {
		numbersString = ns.args[0].toString()
		targetNumber = parseInt(ns.args[1])
	} else {
		numbersString = arrayInput[0]
		targetNumber = arrayInput[1]
	}

	var returnString = ""
	var resultArray = []

	// we'll need a way to iterate over all possible combination of numbers
	// so we're going to use an array with n elements. Each element is a number with value of n. n = length of input string. 
	// this way we can split up the input string into chunks of numbers and then test if they can be made into the target number
	var n = numbersString.length
	var nArray = []

	for (var i = 0; i < n; i++) {
		nArray.push(n)
	}

	var numbersIntArray = returnSlicedNumbers(numbersString)

	for (var numbersInt of numbersIntArray) {
		// creating the base 3 number we're going to use for selecting the math operators
		var numOperators = numbersInt.length - 1
		var operatorsString = ""
		for (let i = 0; i < numOperators; i++) {
			operatorsString += "2"
		}

		var operatorsBase3 = parseInt(operatorsString, 3) // string with all the operators. first will be filled with numbers. then will be replaced with operators. 
		var operatorsBase3MaxLength = operatorsBase3.toString(3).split('').length // get the amount of operators we'll need
		while (operatorsBase3 >= 0) { // iterate over each operator combination
			if (debug) await ns.write(debugFile, "\n")
			if (debug) await ns.write(debugFile, " ----- " + operatorsBase3.toString(3) + " ----- ")
			if (debug) await ns.write(debugFile, "\n")

			var operatorsArray = operatorsBase3.toString(3).split('')

			// fill array with leading zeros
			while (operatorsArray.length < operatorsBase3MaxLength) {
				operatorsArray.unshift("0")
			}

			// replace numbers with operators
			for (let i = 0; i < operatorsArray.length; i++) {
				if (operatorsArray[i] == "0") operatorsArray[i] = "+"
				if (operatorsArray[i] == "1") operatorsArray[i] = "-"
				if (operatorsArray[i] == "2") operatorsArray[i] = "*"
			}

			if (debug) await ns.write(debugFile, "operators: " + operatorsArray)
			if (debug) await ns.write(debugFile, "\n")

			// first we do the multiplication
			// if an * operator is found, both values are multiplied and the result will replace both values
			var subtotal = 0
			var numbers = numbersInt.slice(0) // make a copy of the input numbers, so we can work with the array
			var operators = operatorsArray.slice(0) // also make a copy of the operators

			if (debug) await ns.write(debugFile, "numbers: " + numbers)
			if (debug) await ns.write(debugFile, "\n")

			// iterate through the operators from right to left (biggest to smallest number)
			for (var i = operatorsArray.length; i >= 0; i--) {
				var currentOperator = operatorsArray[i]
				if (currentOperator == "*") {
					subtotal = numbers[i] * numbers[i + 1] // multiply both numbers
					if (debug) await ns.write(debugFile, subtotal + " = " + numbers[i] + " " + currentOperator + " " + numbers[i + 1])
					if (debug) await ns.write(debugFile, "\n")
					numbers[i] = subtotal // replace first number with the product of both numbers
					numbers.splice(i + 1, 1) // remove second number
					operators.splice(i, 1) // also remove the operator we just used
				}
			}

			if (debug) await ns.write(debugFile, "numbers after *: " + numbers)
			if (debug) await ns.write(debugFile, "\n")
			if (debug) await ns.write(debugFile, "operators after *: " + operators)
			if (debug) await ns.write(debugFile, "\n")

			// do substraction and addition next
			for (var i = operators.length; i >= 0; i--) {
				var currentOperator = operators[i]
				if (currentOperator == "+") {
					subtotal = numbers[i] + numbers[i + 1]
					if (debug) await ns.write(debugFile, subtotal + " = " + numbers[i] + " " + currentOperator + " " + numbers[i + 1])
					if (debug) await ns.write(debugFile, "\n")
					numbers[i] = subtotal
					numbers.splice(i + 1, 1)
					operators.splice(i)
				}
				if (currentOperator == "-") {
					subtotal = numbers[i] - numbers[i + 1]
					if (debug) await ns.write(debugFile, subtotal + " = " + numbers[i] + " " + currentOperator + " " + numbers[i + 1])
					if (debug) await ns.write(debugFile, "\n")
					numbers[i] = subtotal
					numbers.splice(i + 1, 1)
					operators.splice(i)
				}
			}

			if (debug) await ns.write(debugFile, "numbers after +/-: " + numbers)
			if (debug) await ns.write(debugFile, "\n")

			if (numbers[0] == targetNumber) { // we found a combination of operators that produces the desired result !!! 
				if (debug) await ns.write(debugFile, " > > > target found!")
				if (debug) await ns.write(debugFile, "\n")
				if (debug) await ns.write(debugFile, "operators: " + operatorsArray)
				if (debug) await ns.write(debugFile, "\n")
				if (debug) await ns.write(debugFile, "numbers: " + numbersInt)
				if (debug) await ns.write(debugFile, "\n")
				var numbersAndOperatorsArray = []

				// iterate through all the original numbers and operators to build the final result string
				for (i = 0; i < numbersInt.length; i++) {
					if (i == numbersInt.length - 1) { // for the last number, we only push the number and no operator
						numbersAndOperatorsArray.push(numbersInt[i])
					} else {
						numbersAndOperatorsArray.push(numbersInt[i])
						numbersAndOperatorsArray.push(operatorsArray[i])
					}
				}
				resultArray.push(numbersAndOperatorsArray)
			}

			operatorsBase3--
		}
	}

	// output should be a nicely formatted array, not just a series of numbers
	returnString += "["
	for (var row = 0; row < resultArray.length; row++) {
		for (var col = 0; col < resultArray[row].length; col++) {
			returnString += resultArray[row][col]
		}
		if (row != resultArray.length - 1) returnString += ", "
	}
	returnString += "]"

	ns.tprint("output:   " + returnString)
	ns.print("finished")
}

function returnSlicedNumbers(number) {
	// takes a number as a string and returns all possible slice combinations. e.g.: 
	// number = "123"
	// result = [[1,2,3],[1,23],[12,3],[123]]

	var numbersString = number // the input string we're supposed to work with
	var bitPatternString = "" // a bit pattern used to slice up the number string
	var numbersArrayFinal = [] // the array to hold all the possible combinations of sliced numbers

	for (let i = 1; i < numbersString.length; i++) { // we start at 1 so we get a bit pattern that is 1 less then the string length
		bitPatternString += "1" // we fill it with "1"s so we esentially start with the highest number and simply count down
	}
	var bitPattern = parseInt(bitPatternString, 2) // convert it into a number so we can count it down

	while (bitPattern >= 0) {
		bitPatternString = bitPattern.toString(2) // we again convert the number to a string ...
		while (bitPatternString.length < numbersString.length - 1) { // ... so we can fill it with leading zeros later
			bitPatternString = "0" + bitPatternString
		}

		// now we have a numbersString like this "63512" and a bitPatternString like this "1111" (1 less in length than the numbers string)
		// all we have to do now is count the bitPatternString down and use the bits to slice the numbersString

		var numbersArray = [] // holds the current sliced parts
		var slicePositionEnd = 1 // position we're going to slice at
		var slicePositionStart = 0 // 
		var bitPatternPointer = 0 // points to the position of the bitPatternString string

		while (bitPatternPointer < bitPatternString.length) {
			if (bitPatternString.charAt(bitPatternPointer) == "0") {
				// no need to slice. just do nothing. 
			} else {
				// slice the corresponding part of the string, convert it to int and push it into the array
				var subString = numbersString.slice(slicePositionStart, slicePositionEnd)
				numbersArray.push(parseInt(subString))
				slicePositionStart = slicePositionEnd
			}

			slicePositionEnd += 1
			bitPatternPointer += 1
		}
		if (slicePositionStart != slicePositionEnd) { // if we're out of the loop, but there are still characters left, also push them into the array
			var subString = numbersString.slice(slicePositionStart, slicePositionEnd)
			numbersArray.push(parseInt(subString))
		}

		numbersArrayFinal.push(numbersArray)
		// we not have an array (numbersArray) with all possible slice combinations of numbers

		bitPattern--
	}

	// clean up the array from invalid expressions, e.g. if input number is "105" one of the combinations would be [1, 05]. Since 05 can not exist, we remove it
	for (let i = numbersArrayFinal.length - 1; i >= 0; i--) {
		var totalLength = 0
		for (let j of numbersArrayFinal[i]) {
			totalLength += j.toString().length
		}
		// if the total length of the numbers is not equal to the original number, then we're missing numbers (leading zeros) and the slice is invalid
		if (totalLength < numbersString.length) {
			numbersArrayFinal.splice(i, 1)
		}
	}

	return numbersArrayFinal
}